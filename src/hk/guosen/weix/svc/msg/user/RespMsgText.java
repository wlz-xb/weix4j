package hk.guosen.weix.svc.msg.user;

import hk.guosen.weix.svc.msg.MsgType.RespType;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 文本应答消息 
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
@XStreamAlias("xml")
public class RespMsgText extends RespMsg
{
	
	// 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示） 
    @XStreamAlias("Content")
	private String content;

	public RespMsgText(BaseMsg req, String content)
	{
		super(req, RespType.text.name());
		this.content = content;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}
 
}
