package hk.guosen.weix.svc.msg.user;

/**
 * 文本请求消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class ReqMsgText extends ReqMsg
{
	//请求内容
	private String content;

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}
}
