package hk.guosen.weix.svc.msg.user;

/**
 * 图片请求消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class ReqMsgImage extends ReqMsgMedia
{
	// 图片链接   
    private String picUrl;
    
	public String getPicUrl()
	{
		return picUrl;
	}

	public void setPicUrl(String picUrl)
	{
		this.picUrl = picUrl;
	}
}
