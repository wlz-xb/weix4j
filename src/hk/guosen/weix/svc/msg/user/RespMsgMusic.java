package hk.guosen.weix.svc.msg.user;

import hk.guosen.weix.shared.msg.Music;
import hk.guosen.weix.svc.msg.MsgType.RespType;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复音乐消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
@XStreamAlias("xml")
public class RespMsgMusic extends RespMsg
{
	@XStreamAlias("Music")
	private Music music;
    
    public RespMsgMusic(BaseMsg req, Music music)
    {
    	super(req, RespType.music.name());
    	this.music = music;
    }

	public Music getMusic()
	{
		return music;
	}

	public void setMusic(Music music)
	{
		this.music = music;
	}
    
    
}
