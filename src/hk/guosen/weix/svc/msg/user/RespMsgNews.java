package hk.guosen.weix.svc.msg.user;

import hk.guosen.weix.shared.msg.Article;
import hk.guosen.weix.svc.msg.MsgType.RespType;

import java.util.Arrays;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复图文消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
@XStreamAlias("xml")
public class RespMsgNews extends RespMsg
{
	@XStreamAlias("Articles")
	private List<Article> articles;
	@XStreamAlias("ArticleCount")
	private int articleCount;

	public RespMsgNews(BaseMsg req, List<Article> articles)
	{
		super(req, RespType.news.name());
		setArticles(articles);
		this.articleCount = articles.size();
	}
	
	public RespMsgNews(BaseMsg req, Article...articles)
	{
		this(req, Arrays.asList(articles));
	}

	public List<Article> getArticles()
	{
		return articles;
	}

	public void setArticles(List<Article> articles)
	{
		this.articles = articles;
		this.articleCount = articles.size();
	}

	public int getArticleCount()
	{
		return articleCount;
	}
}
