package hk.guosen.weix.svc;

import hk.guosen.weix.svc.msg.user.BaseMsg;
import hk.guosen.weix.svc.msg.user.RespMsg;

/**
 * 应用服务接口。
 * 应用程序通过实现此接口，处理微信客户请求及微信的系统事件。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public interface AppService
{
	/**
	 * 微信的请求到达时，此方法将被调用。
	 * 
	 * 微信过来的请求分为两类。
	 * 一类为用户请求，一般需要应答。
	 * 一类为事件请求，无需应答。
	 * 
	 * @param msg 请求消息
	 * @return 应答消息, 可为空。
	 * 
	 * @throws Exception
	 */
	RespMsg onMsg(BaseMsg msg) throws Exception;
}
