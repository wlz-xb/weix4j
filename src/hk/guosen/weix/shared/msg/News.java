package hk.guosen.weix.shared.msg;

import java.util.Arrays;
import java.util.List;

/**
 * 图文消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class News
{
	public List<Article> articles;

	public News(List<Article> articles)
	{
		this.articles = articles;
	}
	
	public News(Article... article)
	{
		this(Arrays.asList(article));
	}

}
