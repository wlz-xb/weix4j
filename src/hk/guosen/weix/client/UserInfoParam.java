package hk.guosen.weix.client;

/**
 * 获取用户信息的参数
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class UserInfoParam extends AccessTokenParam
{
	private String openid;

	public String getOpenid()
	{
		return openid;
	}

	public void setOpenid(String openid)
	{
		this.openid = openid;
	}
	
	public UserInfoParam(AccessTokenParam ac, String openid)
	{
		super(ac.getAccess_token());
		setOpenid(openid);
	}
	

}
