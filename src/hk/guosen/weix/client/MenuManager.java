package hk.guosen.weix.client;

import hk.guosen.weix.client.msg.BaseResp;
import hk.guosen.weix.client.msg.menu.GetMenuResp;
import hk.guosen.weix.client.msg.menu.Menu;

/**
 * 菜单管理接口
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public interface MenuManager
{
	/**
	 * 创建菜单
	 * @param menu
	 * @return
	 */
	BaseResp createMenu(Menu menu);
	/**
	 * 删除菜单
	 * @return
	 */
	BaseResp deleteMenu();
	/**
	 * 获取菜单信息
	 * @return
	 */
	GetMenuResp getMenu();
}
