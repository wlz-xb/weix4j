package hk.guosen.weix.client.msg.menu;

import hk.guosen.weix.client.msg.BaseResp;

/**
 * 获取菜单消息的应答。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class GetMenuResp extends BaseResp
{
	private Menu menu;

	public Menu getMenu()
	{
		return menu;
	}

	public void setMenu(Menu menu)
	{
		this.menu = menu;
	}
}
