package hk.guosen.weix.client.msg;
/**
 * 微信返回的错误信息,其他返回消息从此消息继承。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class BaseResp
{
	public Long errcode;
	public String errmsg;
	
	/**
	 * 请求是否成功
	 * 
	 * @return
	 */
	public boolean success()
	{
		return errcode == null || errcode == 0;
	}

	@Override
	public String toString()
	{
		return "BaseResp [errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
}
