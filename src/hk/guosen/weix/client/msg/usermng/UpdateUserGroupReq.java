package hk.guosen.weix.client.msg.usermng;

/**
 * 修改用户分组请求
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class UpdateUserGroupReq
{
	// 用户唯一标识符
	public String openid;
	// 分组id
	public long to_groupid;
	
	public UpdateUserGroupReq(String openid, long to_groupid)
	{
		this.openid = openid;
		this.to_groupid = to_groupid;
	}  
}
