package hk.guosen.weix.client.msg.usersvc;

import hk.guosen.weix.shared.msg.Media;

/**
 * 发送给客户的语音信息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class UserMsgVoice extends UserMsg
{
	public Media voice;
	
	public UserMsgVoice(String touser, String media_id)
	{
		super(touser, "voice");
		this.voice = new Media(media_id);
	}
}
