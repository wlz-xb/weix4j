package hk.guosen.weix.client.msg.usersvc;

import hk.guosen.weix.shared.msg.News;


/**
 * 发送给客户的图文信息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class UserMsgNews extends UserMsg
{
	public News news;

	public UserMsgNews(String touser, News news)
	{
		super(touser, "news");
		this.news = news;
	}

}
